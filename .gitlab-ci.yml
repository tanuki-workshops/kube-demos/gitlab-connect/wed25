stages:
  - 🔎code-quality
  - 🔎secure
  - 🧨unit-tests
  - 🐳build
  - 🚢deploy

variables:
  CONTAINER_PORT: 8080
  EXPOSED_PORT: 80

include:
  - project: 'simplify-devops/kube-helpers'
    file: 'k8s.dsl.yml'
  - project: 'simplify-devops/mocha'
    file: 'mocha.gitlab-ci.yml'
  - project: 'simplify-devops/eslint'
    file: 'eslint.gitlab-ci.yml'
  - project: 'simplify-devops/git-quick-stats'
    file: 'git-quick-stats.gitlab-ci.yml'
  - project: 'simplify-devops/semgrep'
    file: 'semgrep.gitlab-ci.yml'
  - project: 'simplify-devops/gitleaks'
    file: 'gitleaks.gitlab-ci.yml'
  - project: 'simplify-devops/kaniko'
    file: 'kaniko.gitlab-ci.yml'

#-----------------------------------------------------------------------------------------
# Quality
#-----------------------------------------------------------------------------------------
🔎:code:quality:eslint:
  stage: 🔎code-quality
  extends: .eslint:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

🤖:git-analysis:
  stage: 🔎code-quality
  extends: .git-quick-stats:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

#-----------------------------------------------------------------------------------------
# Security
#-----------------------------------------------------------------------------------------
👮‍♀️:vulnerability:detection:
  stage: 🔎secure
  extends: .semgrep:analyzer  
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

👮‍♀️:secrets:detection:
  stage: 🔎secure
  extends: .gitleaks:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

#-----------------------------------------------------------------------------------------
# Tests report in the MR
#-----------------------------------------------------------------------------------------
🎯:mocha:testing:
  stage: 🧨unit-tests
  extends: .mocha:testing
  rules:
    - if: $CI_MERGE_REQUEST_IID

#-----------------------------------------------------------------------------------------
# Build Docker image with Kaniko
#-----------------------------------------------------------------------------------------
📦:kaniko:build:
  stage: 🐳build
  extends: .kaniko:builder
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

#-----------------------------------------------------------------------------------------
# Deploy
#-----------------------------------------------------------------------------------------
🚀:production:deploy:
  stage: 🚢deploy
  extends: .k8s:dsl
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  environment:
    name: production/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
  script:
    - generate_manifest
    - cat /kube/simple.web.app.${CI_COMMIT_SHORT_SHA}.yaml
    - apply
    - scale 3

❌:stop:production:deploy:
  stage: 🚢deploy
  extends: .k8s:dsl
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: manual
  allow_failure: true
  environment:
    name: production/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    action: stop
  script:
    - generate_manifest
    - delete_deployment

#-----------------------------------------------------------------------------------------
# Deploy Review Application
#-----------------------------------------------------------------------------------------
🎉:preview:deploy:
  stage: 🚢deploy
  extends: .k8s:dsl
  rules:
    - if: $CI_MERGE_REQUEST_IID
  environment:
    name: preview/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    url: http://${CI_PROJECT_NAME}.${CI_COMMIT_REF_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
    on_stop: 😢:stop:preview:deploy
  script:
    - generate_manifest
    - apply

😢:stop:preview:deploy:
  stage: 🚢deploy
  extends: .k8s:dsl
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  allow_failure: true
  environment:
    name: preview/${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
    action: stop
  script:
    - generate_manifest
    - delete_deployment

