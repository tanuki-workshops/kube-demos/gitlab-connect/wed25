// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true })
const path = require('path')
//const fetch = require('node-fetch')

const fancy = require('./fancy')
const sensors = require('./sensors')

let cmd = "var toto=42"
eval(cmd)

let temperatureComponent = new sensors.Component({
  minValue:-5.0, 
  maxValue:38.0, 
  name: 'temperature', 
  help: 'temperature generator',
  intervall: 5000
})

let humidityComponent = new sensors.Component({
  minValue:0.0, 
  maxValue:100.0, 
  name: 'humidity', 
  help: 'humidity generator',
  intervall: 5000
})

temperatureComponent.start()
humidityComponent.start()

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/'
})

fastify.register(require('./routes'), {
  message: "hello",
  podName: fancy.fancyName(), // get a name for the current pod
  environment: {
    name: process.env.CI_ENVIRONMENT_NAME,
    slug: process.env.CI_ENVIRONMENT_SLUG,
    url: process.env.CI_ENVIRONMENT_URL
  },
  temperatureComponent: temperatureComponent,
  humidityComponent: humidityComponent
})

// fastify.register(require('fastify-redis'), { url: constants.redisUrl, /* other redis options */ })

// Run the server!
const start = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
    
  } catch (error) {
    fastify.log.error(error)
    //process.exit(1)
  }
}
start()
